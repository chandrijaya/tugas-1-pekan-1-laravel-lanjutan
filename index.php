<!-- Chandra Aldiwijaya -->
<!-- chandra.aldiwijaya.694@gmail.com -->
<!-- Tugas 1 Pekan 1 Laravel Lanjutan -->
<!Doctype html>
<html>
    <head>
        <title>Pendalaman OOP PHP</title>
    </head>
    <body>
        <section>
            <div>
                <h1>Pendalaman OOP PHP</h1>
                <?php
                    require_once 'main.php'; 
                    require_once 'elang.php'; 
                    require_once 'harimau.php'; 
                    
                    $elang = new elang("Taka");
                    $harimau = new harimau("Tora");
                    
                    echo "<h3> Informasi Hewan </h3><hr>";
                    echo "<h5> Informasi Elang </h5>";
                    $elang->getInfoHewan();
                    echo "<h5> Informasi Harimau </h5>";
                    $harimau->getInfoHewan();
                
                    echo "<h3> Atraksi Hewan </h3><hr>";
                    $elang->atraksi();
                    $harimau->atraksi();
                    
                    echo "<h3> Duel Hewan </h3><hr>";
                    for ($i=1; $i<=100; $i++) {
                        echo "<h5>Round ke-$i</h5>";
                        $elang->serang($harimau);
                        if ($harimau->darah <= 0) {
                            echo "<h3> Pemenangnya adalah $elang->nama!</h3><hr>";
                            break;
                        }
                        $harimau->serang($elang);
                        if ($elang->darah <= 0) {
                            echo "<h3> Pemenangnya adalah $harimau->nama!</h3><hr>";
                            break;
                        }
                    }
                ?>
            </div>
        </section>
    </body>
</html>
